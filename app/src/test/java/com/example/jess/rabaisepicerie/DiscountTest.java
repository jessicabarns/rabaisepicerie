package com.example.jess.rabaisepicerie;

import com.example.jess.rabaisepicerie.beans.Discount;

import org.joda.time.DateTime;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class DiscountTest {
    Discount discount;

    @Test
    public void getRemainingDaysTest(){
        DateTime start = createPastDateTime(7);
        DateTime end = new DateTime();
        discount = createDiscount(start, end);
        assertTrue(discount.getRemainingDays() == 0);
    }

    @Test
    public void getRemainingDaysPastTest(){
        DateTime start = createPastDateTime(7);
        DateTime end = createPastDateTime(1);
        discount = createDiscount(start, end);
        assertTrue(discount.getRemainingDays() == -1);
    }

    @Test
    public void getRemainingDaysFutureTest(){
        DateTime start = createPastDateTime(7);
        DateTime end = createFutureDateTime(7);
        discount = createDiscount(start, end);
        assertTrue(discount.getRemainingDays() == 7);
    }

    @Test
    public void isValidTest(){
        DateTime start = createPastDateTime(7);
        DateTime end = new DateTime();
        discount = createDiscount(start, end);
        assertTrue(discount.isValid());
    }

    @Test
    public void isNotValidTest(){
        DateTime start = createPastDateTime(7);
        DateTime end = createPastDateTime(1);
        discount = createDiscount(start, end);
        assertFalse(discount.isValid());
    }

    private DateTime createPastDateTime(int daysInPast){
        DateTime dateTime = new DateTime();
        dateTime = dateTime.minusDays(daysInPast);
        return dateTime;
    }

    private DateTime createFutureDateTime(int daysToAdd){
        DateTime dateTime = new DateTime();
        dateTime = dateTime.plusDays(daysToAdd);
        return dateTime;
    }

    private Discount createDiscount(DateTime startDate, DateTime endDate){
        return new Discount(10, 10, startDate, endDate);
    }
}
