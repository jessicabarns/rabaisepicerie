package com.example.jess.rabaisepicerie.fragments;

import android.support.v4.app.Fragment;

import com.example.jess.rabaisepicerie.beans.Item;
import com.example.jess.rabaisepicerie.beans.Store;

public abstract class AppFragment extends Fragment {
    protected String title;
    protected String name;

    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public abstract void init();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract void removeItemFromAllLists(Item item);

    public abstract void showStoreItems(Store store);

    public abstract void onRemoveItemFromFavorites(Item item);

    public abstract void onAddItemToFavorites(Item item);

    public abstract boolean isDynamic();

    public abstract AppFragment getCurrentFragment();

    public abstract void showStores();
}
