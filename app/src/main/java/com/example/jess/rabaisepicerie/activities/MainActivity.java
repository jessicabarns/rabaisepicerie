package com.example.jess.rabaisepicerie.activities;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.jess.rabaisepicerie.R;
import com.example.jess.rabaisepicerie.beans.Item;
import com.example.jess.rabaisepicerie.beans.Store;
import com.example.jess.rabaisepicerie.beans.Stores;
import com.example.jess.rabaisepicerie.adapters.ViewPagerAdapter;
import com.example.jess.rabaisepicerie.fragments.DiscountsFragment;
import com.example.jess.rabaisepicerie.fragments.DiscountsStoreItemsFragment;
import com.example.jess.rabaisepicerie.persist.DatabaseHelper;
import com.example.jess.rabaisepicerie.utils.Fetch;
import com.example.jess.rabaisepicerie.utils.Globals;
import com.example.jess.rabaisepicerie.utils.JSONParser;
import com.example.jess.rabaisepicerie.fragments.AppFragment;
import com.example.jess.rabaisepicerie.fragments.HomeFragment;
import com.example.jess.rabaisepicerie.fragments.ListFragment;
import com.example.jess.rabaisepicerie.utils.Utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import static com.example.jess.rabaisepicerie.utils.Globals.Constantes.PREFERENCES_TABLE_NAME;
import static com.example.jess.rabaisepicerie.utils.Globals.Constantes.PREFERENCES_TABLE_STORE_NAME_FIELD;
import static com.example.jess.rabaisepicerie.utils.Globals.stores;

public class MainActivity extends AppCompatActivity implements Fetch.FetchInterface {
    private static ViewPager viewPager;
    private static TabLayout tabLayout;
    private static ViewPagerAdapter viewPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewPager = (ViewPager) findViewById(R.id.viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);

        Toolbar menu = (Toolbar) findViewById(R.id.menu);
        setSupportActionBar(menu);

        initViewPager();

        if (stores == null) {
            fetchStores();
        } else {
            try {
                setViewPagerFragmentsData();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static boolean isCurrentFragment(AppFragment fragment) {
        return getCurrentFragment(true).getName().equals(fragment.getName());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.preferences:
                toPreferencesActivity();
                return true;
            case R.id.refresh:
                fetchStores();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void toPreferencesActivity() {
        startActivity(new Intent(this, PreferencesActvity.class));
    }

    public void fetchStores() {
        Fetch fetch = new Fetch(MainActivity.this, this, Globals.Constantes.JSON_URL);
        fetch.execute();
    }

    private void initViewPager() {
        List<AppFragment> fragments = new ArrayList<>();

        HomeFragment homeFragment = new HomeFragment();
        homeFragment.setTitle(getString(R.string.home_title));
        homeFragment.setName("home");
        fragments.add(homeFragment);

        DiscountsFragment discountsFragment = new DiscountsFragment();
        discountsFragment.setTitle(getString(R.string.discounts_title));
        discountsFragment.setName("discounts");
        fragments.add(discountsFragment);

        ListFragment listFragment = new ListFragment();
        listFragment.setTitle(getString(R.string.list_title));
        listFragment.setName("list");
        fragments.add(listFragment);

        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), fragments);
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onResponse(String response) {
        JSONParser jsonParser = new JSONParser(response);
        stores = new Stores(jsonParser.getStores());
        try {
            setViewPagerFragmentsData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static AppFragment getCurrentFragment(boolean includeChilds) {
        AppFragment currentFragment = (AppFragment) viewPagerAdapter.getItem(viewPager.getCurrentItem());
        if (includeChilds && currentFragment.isDynamic()) {
            return currentFragment.getCurrentFragment();
        }
        return currentFragment;
    }

    public void setViewPagerFragmentsData() throws Exception {
        if (!DatabaseHelper.arePreferencesSet(this)) {
            toPreferencesActivity();
        } else {
            setFavoriteItems();
            refreshStores(DatabaseHelper.getPreferences(this));
        }
    }

    public static void refreshStores(Set<String> preferences) throws Exception {
        setStoresVisibility(preferences);
        viewPagerAdapter.initFragments();
    }

    private void setFavoriteItems() throws Exception {
        Map<String, String> items = DatabaseHelper.getFavoriteItems(this);

        Item item;
        for (Map.Entry<String, String> entry : items.entrySet())
        {
            item = Globals.stores.getItemById(entry.getKey());
            if(item != null && Globals.stores.getItemStore(item).getName().equals(entry.getValue())){
               item.setFavorite(true);
            }
        }
    }

    private static void setStoresVisibility(Set<String> preferences) {
        for (Store store : stores.getStores()) {
            if (preferences.contains(store.getName())) {
                store.setVisible(true);
            }
        }
    }

    protected void addToFavorites(View view) throws Exception {
        Item item = (Item) view.getTag();
        addToFavorites(item);
        DatabaseHelper.saveToFavorites(this, item);
    }

    public static void addToFavorites(Item item) {// TODO: SQLite favoris
        item.setFavorite(true);
        for (AppFragment fragment : viewPagerAdapter.getFragments()) {
            fragment.onAddItemToFavorites(item);
        }
    }

    protected void onSeeItemClick(View view) {
        Item item = (Item) view.getTag();
        toItemDesctiptionActivity(item);
    }

    private void toItemDesctiptionActivity(Item item) {
        Intent intent = new Intent(this, ItemDescriptionActivity.class);
        intent.putExtra("itemId", item.getId());
        startActivity(intent);
    }

    protected void onSeeStoreClick(View view) {
        Store store = (Store) view.getTag();
        getCurrentFragment(false).showStoreItems(store);
    }

    protected void onBackPressed(View view) {
        ViewPagerAdapter viewPagerAdapter = (ViewPagerAdapter) viewPager.getAdapter();
        getCurrentFragment(false).showStores();
    }

    @Override
    public void onBackPressed() {
        if (getCurrentFragment(true).getName().equals("discountStoreItems")) {
            ((DiscountsFragment) getCurrentFragment(false)).showStores();
        } else if (getCurrentFragment(true).getName().equals("listStoreItems")) {
            ((ListFragment) getCurrentFragment(false)).showStores();
        } else {
            super.onBackPressed();
        }
    }

    protected void removeStoreItemsFromFavorites(View view) throws Exception {
        Store store = (Store) view.getTag();

        List<Item> unfavoritedItems = store.getFavoriteItems();
        store.unfavoriteAllItems();

        for (AppFragment fragment : viewPagerAdapter.getFragments()) {
            for (Item item : unfavoritedItems) {
                fragment.onRemoveItemFromFavorites(item);
                DatabaseHelper.removeItemFromFavorites(this, item);
            }
        }
    }

    protected void removeItemFromFavorites(View view) throws Exception {
        Item item = (Item) view.getTag();
        removeFromFavorites(item);
        DatabaseHelper.removeItemFromFavorites(this, item);
    }

    public static void removeFromFavorites(Item item) {
        item.setFavorite(false);
        for (AppFragment fragment : viewPagerAdapter.getFragments()) {
            fragment.onRemoveItemFromFavorites(item);
        }
    }

    protected void showAllFavoriteItems(View view) {
        ((ListFragment) getCurrentFragment(false)).showAllItems();
    }
}
