package com.example.jess.rabaisepicerie.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.example.jess.rabaisepicerie.R;
import com.example.jess.rabaisepicerie.adapters.ItemListAdapter;
import com.example.jess.rabaisepicerie.beans.Item;
import com.example.jess.rabaisepicerie.beans.Store;

public class DiscountsStoreItemsFragment extends AppFragment {
    private static TextView title;
    private static ListView itemsListView;
    private static ItemListAdapter itemListAdapter;
    private static Store store;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.discounts_store_items_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        View view = getView();
        title = (TextView) view.findViewById(R.id.title_discounts);
        itemsListView = (ListView) view.findViewById(R.id.items_list_discounts);
        itemsListView.setEmptyView(view.findViewById(R.id.empty_list_discounts));

        /*if(stores != null){
            init();
        }*/
    }

    @Override
    public void init() {
        if(this.getContext() != null){
            title.setText(store.getName());
            itemListAdapter = new ItemListAdapter(store.getNonFavoriteItems(), false);
            itemsListView.setAdapter(itemListAdapter);
        }
    }

    @Override
    public void removeItemFromAllLists(Item item) {
        itemListAdapter.remove(item);
    }

    @Override
    public void showStoreItems(Store store) {

    }

    @Override
    public void onRemoveItemFromFavorites(Item item) {
        itemListAdapter.add(item);
    }

    @Override
    public void onAddItemToFavorites(Item item) {
        itemListAdapter.remove(item);
    }

    @Override
    public boolean isDynamic() {
        return false;
    }

    @Override
    public AppFragment getCurrentFragment() {
        return null;
    }

    @Override
    public void showStores() {

    }

    public void setStore(Store store) {
        DiscountsStoreItemsFragment.store = store;
    }

    public boolean isEmpty(){
        return itemListAdapter.getCount() == 0;
    }
}
