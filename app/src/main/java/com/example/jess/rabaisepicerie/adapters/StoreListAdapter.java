package com.example.jess.rabaisepicerie.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.jess.rabaisepicerie.R;
import com.example.jess.rabaisepicerie.beans.Item;
import com.example.jess.rabaisepicerie.beans.Store;

import java.util.List;

public class StoreListAdapter extends BaseAdapter {
    private List<Store> stores;
    private LayoutInflater inflater;
    private boolean removeButton;
    private boolean showFavoritesItems;

    public StoreListAdapter(List<Store> stores, boolean removeButton, boolean showFavoritesItems) {
        this.stores = stores;
        this.removeButton = removeButton;
        this.showFavoritesItems = showFavoritesItems;
    }

    @Override
    public int getCount() {
        return stores.size();
    }

    @Override
    public Store getItem(int position) {
        return stores.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(inflater == null){
            Context context = parent.getContext();
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        View row = inflater.inflate(R.layout.store_list_view, parent, false);

        Store store = getItem(position);

        TextView name = (TextView) row.findViewById(R.id.name);
        TextView numberOfItems = (TextView) row.findViewById(R.id.number_of_items);
        ImageButton see = (ImageButton) row.findViewById(R.id.see);

        see.setTag(store);

        name.setText(store.getName());

        int items = (showFavoritesItems ? store.getNumberOfFavoritesItems() : store.getNumberOfNonFavoriteItems());
        String numberofItemsStr = inflater.getContext().getString(R.string.number_of_items, items);
        numberOfItems.setText(numberofItemsStr);

        if(removeButton){
            ImageButton remove = (ImageButton) row.findViewById(R.id.remove);
            remove.setVisibility(View.VISIBLE);
            remove.setTag(store);
        }

        return (row);
    }

    public void remove(Store store) {
        stores.remove(store);
        notifyDataSetChanged();
    }
}
