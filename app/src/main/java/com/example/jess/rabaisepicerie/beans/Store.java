package com.example.jess.rabaisepicerie.beans;

import java.util.ArrayList;
import java.util.List;

public class Store {
    private String id;
    private String name;
    private boolean isVisible;
    private List<Item> discountItems;

    public Store(String id, String name, List<Item> discountItems) {
        this.id = id;
        this.name = name;
        this.discountItems = discountItems;
        isVisible = false;
    }

    public void setVisible(boolean isVisible) {
        this.isVisible = isVisible;
    }

    public String getName() {
        return name;
    }

    List<Item> getDiscountItems() {
        return discountItems;
    }

    boolean isVisible() {
        return isVisible;
    }

    public int getNumberOfNonFavoriteItems() {
        return getNonFavoriteItems().size();
    }

    boolean hasFavouritesItems() {
        for (Item item : discountItems) {
            if (item.isFavorite()) {
                return true;
            }
        }
        return false;
    }

    public int getNumberOfFavoritesItems() {
        return getFavoriteItems().size();
    }

    public void unfavoriteAllItems() {
        for (Item item : discountItems) {
            item.setFavorite(false);
        }
    }

    public List<Item> getFavoriteItems() {
        List<Item> items = new ArrayList<>();
        for (Item item : getValidDiscountItems()) {
            if (item.isFavorite()) {
                items.add(item);
            }
        }
        return items;
    }

    List<Item> getValidDiscountItems() {
        List<Item> items = new ArrayList<>();
        for (Item item : discountItems) {
            if (item.isDiscountValid()) {
                items.add(item);
            }
        }
        return items;
    }

    public List<Item> getNonFavoriteItems() {
        List<Item> items = new ArrayList<>();
        for (Item item : getValidDiscountItems()) {
            if (!item.isFavorite()) {
                items.add(item);
            }
        }
        return items;
    }

    @Override
    public String toString() {
        return "Store{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", discountItems=" + discountItems +
                ", isVisible=" + isVisible +
                '}';
    }
}
