package com.example.jess.rabaisepicerie.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jess.rabaisepicerie.persist.DatabaseHelper;

import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;

import static android.graphics.Paint.STRIKE_THRU_TEXT_FLAG;
import static com.example.jess.rabaisepicerie.utils.Globals.Constantes.PREFERENCES_TABLE_NAME;
import static com.example.jess.rabaisepicerie.utils.Globals.Constantes.PREFERENCES_TABLE_STORE_NAME_FIELD;
import static com.example.jess.rabaisepicerie.utils.Globals.Constantes.STORES;

public class Utils {
    public static String getLocaleLanguage(){
        return Locale.getDefault().getLanguage();
    }

    public static String getLocaleCountry(){
        return Locale.getDefault().getCountry();
    }

    public static void makeToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static void strikethrough(TextView textView){
        textView.setPaintFlags(textView.getPaintFlags() | STRIKE_THRU_TEXT_FLAG);
    }
}
