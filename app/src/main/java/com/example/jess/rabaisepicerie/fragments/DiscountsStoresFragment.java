package com.example.jess.rabaisepicerie.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.jess.rabaisepicerie.R;
import com.example.jess.rabaisepicerie.adapters.StoreListAdapter;
import com.example.jess.rabaisepicerie.beans.Item;
import com.example.jess.rabaisepicerie.beans.Store;
import com.example.jess.rabaisepicerie.utils.Globals;

import static com.example.jess.rabaisepicerie.activities.MainActivity.isCurrentFragment;

public class DiscountsStoresFragment extends AppFragment {
    private static ListView storesListView;
    private static StoreListAdapter storeListAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.discounts_stores_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        View view = getView();
        storesListView = (ListView) view.findViewById(R.id.stores_list_discounts);
        storesListView.setEmptyView(view.findViewById(R.id.empty_stores_list_discounts));

        if (Globals.stores != null) {
            init();
        }
    }

    @Override
    public void init() {
        if (this.getContext() != null) {
            storeListAdapter = new StoreListAdapter(Globals.stores.getVisibleStores(), false, false);
            storesListView.setAdapter(storeListAdapter);
        }
    }

    @Override
    public void removeItemFromAllLists(Item item) {

    }

    @Override
    public void showStoreItems(Store store) {

    }

    @Override
    public void onRemoveItemFromFavorites(Item item) {
        if (isCurrentFragment(this)) {
            init();
        }
    }

    @Override
    public void onAddItemToFavorites(Item item) {
        if (isCurrentFragment(this)) {
            init();
        }
    }

    @Override
    public boolean isDynamic() {
        return false;
    }

    @Override
    public AppFragment getCurrentFragment() {
        return null;
    }

    @Override
    public void showStores() {

    }
}
