package com.example.jess.rabaisepicerie.beans;

import android.annotation.TargetApi;
import android.icu.math.BigDecimal;
import android.os.Build;

import org.joda.time.DateTime;

import java.util.Map;

import static com.example.jess.rabaisepicerie.utils.Globals.Constantes.DEFAULT_LANGUAGE;
import static com.example.jess.rabaisepicerie.utils.Utils.getLocaleLanguage;
import static java.math.BigDecimal.ROUND_HALF_EVEN;

public class Item {
    private String id;
    private String imageURL;
    private double discountedPrice;
    private boolean isFavorite;
    private Discount discount;
    private Map<String, String> name;

    public Item(String id, Map<String, String> name, String imageURL, Discount discount) {
        this.id = id;
        this.name = name;
        this.imageURL = imageURL;
        this.discount = discount;
        isFavorite = false;
        calculatePrice();
    }

    @TargetApi(Build.VERSION_CODES.N)
    private void calculatePrice() {
        double discountAmount = discount.getDiscountPercentage() / 100 * discount.getOriginalPrice();
        double notRoundedPrice = discount.getOriginalPrice() - discountAmount;
        BigDecimal bigDecimal = new BigDecimal(notRoundedPrice).setScale(2, ROUND_HALF_EVEN);
        discountedPrice = bigDecimal.doubleValue();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        String result = name.get(getLocaleLanguage());
        if (result == null) {
            result = name.get(DEFAULT_LANGUAGE);
        }
        return name.get(getLocaleLanguage());
    }

    public String getImageURL() {
        return imageURL;
    }

    public double getDiscountedPrice() {
        return discountedPrice;
    }

    public double getDiscountPercentage() {
        return discount.getDiscountPercentage();
    }

    public DateTime getDiscountEndDate() {
        return discount.getEndDate();
    }

    public double getOriginalPrice() {
        return discount.getOriginalPrice();
    }

    public int getDiscountRemainingDays() {
        return discount.getRemainingDays();
    }

    boolean isDiscountValid() {
        return discount.isValid();
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    @Override
    public String toString() {
        return "Item{" +
                "id='" + id + '\'' +
                ", imageURL='" + imageURL + '\'' +
                ", discountedPrice=" + discountedPrice +
                ", isFavorite=" + isFavorite +
                ", discount=" + discount +
                ", name=" + name +
                '}';
    }
}
