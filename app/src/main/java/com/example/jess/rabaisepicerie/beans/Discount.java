package com.example.jess.rabaisepicerie.beans;

import android.util.Log;

import org.joda.time.DateTime;
import org.joda.time.Days;

public class Discount {
    private double originalPrice;
    private double discountPercentage;
    private DateTime startDate;
    private DateTime endDate;

    public Discount(double discountPercentage, double originalPrice, DateTime startDate, DateTime endDate) {
        this.discountPercentage = discountPercentage;
        this.originalPrice = originalPrice;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    double getOriginalPrice() {
        return originalPrice;
    }

    double getDiscountPercentage() {
        return discountPercentage;
    }

    public DateTime getEndDate() {
        return endDate;
    }

    public int getRemainingDays() {
        return Days.daysBetween(new DateTime(), endDate).getDays();
    }

    public boolean isValid() {
        return getRemainingDays() >= 0;
    }

    @Override
    public String toString() {
        return "Discount{" +
                "discountPercentage=" + discountPercentage +
                ", originalPrice=" + originalPrice +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }
}
