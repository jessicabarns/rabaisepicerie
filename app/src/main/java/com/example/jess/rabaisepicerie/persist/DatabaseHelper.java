package com.example.jess.rabaisepicerie.persist;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;

import com.example.jess.rabaisepicerie.beans.Item;
import com.example.jess.rabaisepicerie.utils.Globals;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import static com.example.jess.rabaisepicerie.utils.Globals.Constantes.DATABASE_UPGRADE_UNKNOWN_OLDVERSION_ERROR;
import static com.example.jess.rabaisepicerie.utils.Globals.Constantes.FAVORITES_TABLE_ID_FIELD;
import static com.example.jess.rabaisepicerie.utils.Globals.Constantes.FAVORITES_TABLE_NAME;
import static com.example.jess.rabaisepicerie.utils.Globals.Constantes.FAVORITES_TABLE_STORE_NAME_FIELD;
import static com.example.jess.rabaisepicerie.utils.Globals.Constantes.PREFERENCES_TABLE_NAME;
import static com.example.jess.rabaisepicerie.utils.Globals.Constantes.PREFERENCES_TABLE_STORE_NAME_FIELD;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String TAG = "DatabaseHelper";
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Persistance";

    private static final String SQL_CREATE_TABLE_PREFERENCES = "" +
            "CREATE TABLE IF NOT EXISTS " + PREFERENCES_TABLE_NAME +
            "(" + PREFERENCES_TABLE_STORE_NAME_FIELD + " VARCHAR)";

    private static final String SQL_CREATE_TABLE_FAVORITES = "" +
            "CREATE TABLE IF NOT EXISTS " + FAVORITES_TABLE_NAME +
            "(" + FAVORITES_TABLE_ID_FIELD + " VARCHAR, " +
            "" + FAVORITES_TABLE_STORE_NAME_FIELD + " VARCHAR)";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE_PREFERENCES);
        db.execSQL(SQL_CREATE_TABLE_FAVORITES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        switch (oldVersion) {
            case 1:
                //upgrade logic from version 1 to 2
                break;
            case 2:
                //upgrade lgic from version 2 to 3
                break;
            default:
                String message = String.format(DATABASE_UPGRADE_UNKNOWN_OLDVERSION_ERROR, oldVersion);
                throw new IllegalStateException(message);
        }
    }

    public static boolean arePreferencesSet(AppCompatActivity activity) {
        return getPreferences(activity).size() > 0;
    }

    public static Set<String> getPreferences(AppCompatActivity activity) {
        DatabaseHelper databaseHelper = new DatabaseHelper(activity);
        SQLiteDatabase database = databaseHelper.getReadableDatabase();

        String request = "SELECT * FROM " + PREFERENCES_TABLE_NAME;
        Cursor cursor = database.rawQuery(request, null);

        Set<String> preferences = new TreeSet<>();
        String storeName;
        while (cursor.moveToNext()) {
            storeName = cursor.getString(cursor.getColumnIndex(PREFERENCES_TABLE_STORE_NAME_FIELD));
            preferences.add(storeName);
        }

        cursor.close();
        databaseHelper.close();
        return preferences;
    }

    public static void savePreferences(AppCompatActivity activity, Set<String> storeNames){
        DatabaseHelper databaseHelper = new DatabaseHelper(activity);
        SQLiteDatabase database = databaseHelper.getReadableDatabase();

        ContentValues contentValues;
        for(String storeName : storeNames){
            contentValues = new ContentValues();
            contentValues.put(PREFERENCES_TABLE_STORE_NAME_FIELD, storeName);
            database.insert(PREFERENCES_TABLE_NAME, null, contentValues);
        }

        databaseHelper.close();
    }

    public static void deleteAllFromTable(AppCompatActivity activity, String tableName){
        DatabaseHelper databaseHelper = new DatabaseHelper(activity);
        SQLiteDatabase database = databaseHelper.getReadableDatabase();
        database.delete(tableName, null, null);
        databaseHelper.close();
    }

    public static void saveToFavorites(AppCompatActivity activity, Item item) throws Exception {
        DatabaseHelper databaseHelper = new DatabaseHelper(activity);
        SQLiteDatabase database = databaseHelper.getReadableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(FAVORITES_TABLE_ID_FIELD, item.getId());
        contentValues.put(FAVORITES_TABLE_STORE_NAME_FIELD, Globals.stores.getItemStore(item).getName());

        database.insert(FAVORITES_TABLE_NAME, null, contentValues);

        databaseHelper.close();
    }

    public static void removeItemFromFavorites(AppCompatActivity activity, Item item) throws Exception {
        DatabaseHelper databaseHelper = new DatabaseHelper(activity);
        SQLiteDatabase database = databaseHelper.getReadableDatabase();

        String whereClause = FAVORITES_TABLE_ID_FIELD + " = '"+item.getId()+"' " +
                "AND " + FAVORITES_TABLE_STORE_NAME_FIELD + " = '"+Globals.stores.getItemStore(item).getName()+"'";
        database.delete(FAVORITES_TABLE_NAME, whereClause, null);

        databaseHelper.close();
    }

    public static Map<String,String> getFavoriteItems(AppCompatActivity activity) {
        DatabaseHelper databaseHelper = new DatabaseHelper(activity);
        SQLiteDatabase database = databaseHelper.getReadableDatabase();

        String request = "SELECT * FROM " + FAVORITES_TABLE_NAME;
        Cursor cursor = database.rawQuery(request, null);

        Map<String,String> items = new HashMap<>();
        String id;
        String storeName;
        while (cursor.moveToNext()) {
            id = cursor.getString(cursor.getColumnIndex(FAVORITES_TABLE_ID_FIELD));
            storeName = cursor.getString(cursor.getColumnIndex(FAVORITES_TABLE_STORE_NAME_FIELD));
            items.put(id, storeName);
        }

        cursor.close();
        databaseHelper.close();
        return items;
    }
}
