package com.example.jess.rabaisepicerie.utils;

import com.example.jess.rabaisepicerie.beans.Discount;
import com.example.jess.rabaisepicerie.beans.Item;
import com.example.jess.rabaisepicerie.beans.Store;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import static com.example.jess.rabaisepicerie.utils.Globals.Constantes.DISCOUNT_PERCENTAGE;
import static com.example.jess.rabaisepicerie.utils.Globals.Constantes.END_DATE;
import static com.example.jess.rabaisepicerie.utils.Globals.Constantes.ID;
import static com.example.jess.rabaisepicerie.utils.Globals.Constantes.IMAGE;
import static com.example.jess.rabaisepicerie.utils.Globals.Constantes.ITEMS;
import static com.example.jess.rabaisepicerie.utils.Globals.Constantes.NAME;
import static com.example.jess.rabaisepicerie.utils.Globals.Constantes.ORIGINAL_PRICE;
import static com.example.jess.rabaisepicerie.utils.Globals.Constantes.START_DATE;
import static com.example.jess.rabaisepicerie.utils.Globals.Constantes.STORES;

public class JSONParser {
    private String jsonString;


    public JSONParser(String jsonString) {
        this.jsonString = jsonString;
    }

    public List<Store> getStores() {
        List<Store> stores = new ArrayList<>();

        try {
            JSONArray jsonStores = new JSONObject(jsonString).getJSONArray(STORES);

            Store store;
            JSONObject jsonStore;
            for (int i = 0; i < jsonStores.length(); i++) {
                jsonStore = jsonStores.getJSONObject(i);

                store = getStore(jsonStore);
                stores.add(store);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return stores;
    }

    private Store getStore(JSONObject jsonStore) throws JSONException {
        String id = getStoreId(jsonStore);
        String name = getStoreName(jsonStore);
        List<Item> items = getStoreItems(jsonStore);
        return new Store(id, name, items);
    }

    private String getStoreId(JSONObject jsonStore) throws JSONException {
        return jsonStore.getString(ID);
    }

    private String getStoreName(JSONObject jsonStore) throws JSONException {
        return jsonStore.getString(NAME);
    }

    private List<Item> getStoreItems(JSONObject jsonStore) throws JSONException {
        List<Item> items = new ArrayList<>();
        JSONArray jsonItems = jsonStore.getJSONArray(ITEMS);

        Item item;
        for (int i = 0; i < jsonItems.length(); i++) {
            item = getItem(jsonItems.getJSONObject(i));
            items.add(item);
        }

        return items;
    }

    private Item getItem(JSONObject jsonItem) throws JSONException {
        String id = getItemId(jsonItem);
        Map<String, String> name = getItemName(jsonItem);
        String image = getItemImage(jsonItem);
        Discount discount = getItemDiscount(jsonItem);
        return new Item(id, name, image, discount);
    }

    private String getItemId(JSONObject jsonItem) throws JSONException {
        return jsonItem.getString(ID);
    }

    private Map<String, String> getItemName(JSONObject jsonItem) throws JSONException {
        Map<String, String> name = new HashMap<>();
        JSONObject jsonName = jsonItem.getJSONObject(NAME);

        Iterator langs = jsonName.keys();
        String lang;
        String value;
        while(langs.hasNext()){
            lang = (String) langs.next();
            value = jsonName.getString(lang);
            name.put(lang, value);
        }

        return name;
    }

    private String getItemImage(JSONObject jsonItem) throws JSONException {
        return jsonItem.getString(IMAGE);
    }

    private Discount getItemDiscount(JSONObject jsonItem) throws JSONException {
        int discountPercentage = getItemDiscountPercentage(jsonItem);
        double originalPrice = getItemOriginalPrice(jsonItem);
        DateTime start = getItemDiscountStartDate(jsonItem);
        DateTime end = getItemDiscountEndDate(jsonItem);
        return new Discount(discountPercentage, originalPrice, start, end);
    }

    private int getItemDiscountPercentage(JSONObject jsonItem) throws JSONException {
        return jsonItem.getInt(DISCOUNT_PERCENTAGE);
    }

    private double getItemOriginalPrice(JSONObject jsonItem) throws JSONException {
        return jsonItem.getDouble(ORIGINAL_PRICE);
    }

    private DateTime getItemDiscountStartDate(JSONObject jsonItem) throws JSONException {
        return new DateTime(jsonItem.getString(START_DATE));
    }

    private DateTime getItemDiscountEndDate(JSONObject jsonItem) throws JSONException {
        return new DateTime(jsonItem.getString(END_DATE));
    }
}
