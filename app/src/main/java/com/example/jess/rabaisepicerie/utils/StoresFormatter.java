package com.example.jess.rabaisepicerie.utils;

import android.content.Context;

import com.example.jess.rabaisepicerie.R;
import com.example.jess.rabaisepicerie.beans.Stores;

import java.text.NumberFormat;
import java.util.Locale;

import static com.example.jess.rabaisepicerie.utils.Utils.getLocaleCountry;
import static com.example.jess.rabaisepicerie.utils.Utils.getLocaleLanguage;

public class StoresFormatter {
    private Stores stores;
    private Context context;
    private Locale locale;

    public StoresFormatter(Context context, Stores stores) {
        this.context = context;
        this.stores = stores;
        locale = new Locale(getLocaleLanguage(), getLocaleCountry());
    }


    public String getNumberOfLovedItems() {
        return context.getString(R.string.number_of_items, stores.getNumberOfLovedItems());
    }

    public String getTotalCostOfLovedItems() {
        String cost = NumberFormat.getCurrencyInstance(locale).format(stores.getCostOfLovedItems());
        return context.getString(R.string.total_cost, cost);
    }
}
