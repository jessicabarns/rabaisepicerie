package com.example.jess.rabaisepicerie.utils;

import android.content.Context;

import com.example.jess.rabaisepicerie.R;
import com.example.jess.rabaisepicerie.beans.Item;

import org.joda.time.format.DateTimeFormat;

import java.text.NumberFormat;
import java.util.Locale;

import static com.example.jess.rabaisepicerie.utils.Utils.getLocaleCountry;
import static com.example.jess.rabaisepicerie.utils.Utils.getLocaleLanguage;

public class ItemFormatter {
    private Item item;
    private Locale locale;
    private Context context;

    public ItemFormatter(Context context, Item item){
        this.context = context;
        this.item = item;
        locale = new Locale(getLocaleLanguage(), getLocaleCountry());
    }

    public String getOriginalPrice() {
        return NumberFormat.getCurrencyInstance(locale).format(item.getOriginalPrice());
    }

    public String getPrice() {
        return NumberFormat.getCurrencyInstance(locale).format(item.getDiscountedPrice());
    }

    public String getDiscountPercentage() {
        String discountPercentage = NumberFormat.getPercentInstance(locale).format(item.getDiscountPercentage() / 100);
       return context.getString(R.string.discount, discountPercentage);
    }

    public String getName() {
        return item.getName();
    }

    public String getRemainingDays() {
        String remainingDays = String.valueOf(item.getDiscountRemainingDays());
        return context.getString(R.string.days, remainingDays);
    }

    public String getDiscountEndDate() {
        String endDate = DateTimeFormat.shortDate().print(item.getDiscountEndDate());
        return context.getString(R.string.until, endDate);
    }

    public String getStoreName() throws Exception {
        return Globals.stores.getItemStore(item).getName();
    }
}
