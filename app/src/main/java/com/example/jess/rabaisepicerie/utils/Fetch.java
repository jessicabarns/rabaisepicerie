package com.example.jess.rabaisepicerie.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.jess.rabaisepicerie.R;
import com.example.jess.rabaisepicerie.activities.MainActivity;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Fetch extends AsyncTask<Void, Void, String>{
    private FetchInterface fetchInterface;
    private URL url;
    private ProgressDialog dialog;
    private Context context;

    public interface FetchInterface{
        public void onResponse(String response);
    }

    public Fetch(Context context, FetchInterface fetchInterface, String url){
        this.fetchInterface = fetchInterface;
        this.context = context;

        try {
            this.url = new URL(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = new ProgressDialog(context);
        dialog.setMessage(context.getString(R.string.loading_message));
        dialog.show();
    }

    protected String doInBackground(Void... params) {
        String response = "";
        try {
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) != null)
            {
                stringBuilder.append(line + "\n");
            }

            response = stringBuilder.toString();

            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    protected void onPostExecute(String aVoid) {
        super.onPostExecute(aVoid);
        fetchInterface.onResponse(aVoid);
        dialog.dismiss();
    }
}
