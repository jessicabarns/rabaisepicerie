package com.example.jess.rabaisepicerie.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import com.example.jess.rabaisepicerie.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class ImageLoader extends AsyncTask<Void, Void, Bitmap> {
    private String src;
    private ImageLoaderInterface imageLoaderInterface;
    private ProgressDialog dialog;
    private Context context;

    public interface ImageLoaderInterface {
        public void onResponse(Bitmap image);
    }

    public ImageLoader(Context context, ImageLoaderInterface imageLoaderInterface, String src) {
        this.context = context;
        this.imageLoaderInterface = imageLoaderInterface;
        this.src = src;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = new ProgressDialog(context);
        dialog.setMessage(context.getString(R.string.loading_message));
        dialog.show();
    }

    @Override
    protected Bitmap doInBackground(Void... params) {
        Bitmap image = null;
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            image = BitmapFactory.decodeStream(input);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return image;
    }

    @Override
    protected void onPostExecute(Bitmap aVoid) {
        super.onPostExecute(aVoid);
        imageLoaderInterface.onResponse(aVoid);
        dialog.dismiss();
    }
}
