package com.example.jess.rabaisepicerie.adapters;

import android.content.res.Resources;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import com.example.jess.rabaisepicerie.fragments.AppFragment;

import java.util.List;

public class ViewPagerAdapter extends FragmentPagerAdapter {
    private List<AppFragment> fragments;

    public ViewPagerAdapter(FragmentManager fm, List<AppFragment> fragments) {
        super(fm);
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return fragments.get(position).getTitle();
    }

    public void initFragments() {
        for(AppFragment fragment : fragments){
            fragment.init();
        }
    }

    public List<AppFragment> getFragments() {
        return fragments;
    }

    public AppFragment getItemByName(String name){
        for (AppFragment fragment : fragments){
            if(fragment.getName().equals(name)){
                return fragment;
            }
        }
        throw new Resources.NotFoundException("fragment of name " + name + " doesn't exists in the ViewPagerAdapter");
    }
}
