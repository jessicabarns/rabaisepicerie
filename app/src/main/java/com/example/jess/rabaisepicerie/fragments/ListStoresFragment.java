package com.example.jess.rabaisepicerie.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.example.jess.rabaisepicerie.R;
import com.example.jess.rabaisepicerie.adapters.StoreListAdapter;
import com.example.jess.rabaisepicerie.beans.Item;
import com.example.jess.rabaisepicerie.beans.Store;
import com.example.jess.rabaisepicerie.utils.StoresFormatter;

import static com.example.jess.rabaisepicerie.activities.MainActivity.isCurrentFragment;
import static com.example.jess.rabaisepicerie.utils.Globals.stores;

public class ListStoresFragment extends AppFragment {
    private static ListView storesList;
    private static TextView numberItems;
    private static TextView totalCost;
    private static StoreListAdapter storeListAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list_stores_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        View view = getView();
        storesList = (ListView) view.findViewById(R.id.stores_list);
        storesList.setEmptyView(view.findViewById(R.id.empty_stores_list));
        numberItems = (TextView) view.findViewById(R.id.total_items_list);
        totalCost = (TextView) view.findViewById(R.id.total_cost_list);

        if(stores != null){
            init();
        }
    }

    @Override
    public void init() {
        if(storesList != null && this.getContext() != null){
            storeListAdapter = new StoreListAdapter(stores.getFavouriteStores(), true, true);
            storesList.setAdapter(storeListAdapter);

            StoresFormatter storesFormatter = new StoresFormatter(this.getContext(), stores);
            numberItems.setText(storesFormatter.getNumberOfLovedItems());
            totalCost.setText(storesFormatter.getTotalCostOfLovedItems());
        }
    }

    @Override
    public void removeItemFromAllLists(Item item) {
         if(isCurrentFragment(this)){
             init();
         }
    }

    @Override
    public void showStoreItems(Store store) {

    }

    @Override
    public void onRemoveItemFromFavorites(Item item) {
        if(isCurrentFragment(this)){
            init();
        }
    }

    @Override
    public void onAddItemToFavorites(Item item) {
        if(isCurrentFragment(this)){
            init();
        }
    }

    @Override
    public boolean isDynamic() {
        return false;
    }

    @Override
    public AppFragment getCurrentFragment() {
        return null;
    }

    @Override
    public void showStores() {

    }

    public void removeStoreFromAllLists(Store store) {
        storeListAdapter.remove(store);
    }
}
