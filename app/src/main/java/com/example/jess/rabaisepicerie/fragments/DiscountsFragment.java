package com.example.jess.rabaisepicerie.fragments;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.example.jess.rabaisepicerie.R;
import com.example.jess.rabaisepicerie.beans.Item;
import com.example.jess.rabaisepicerie.beans.Store;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.example.jess.rabaisepicerie.activities.MainActivity.isCurrentFragment;
import static com.example.jess.rabaisepicerie.utils.Globals.stores;

public class DiscountsFragment extends AppFragment {
    private static List<AppFragment> fragments;
    private static FrameLayout frame;
    private static String showingFragmentName;
    private static FragmentActivity activity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.discounts_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        View view = getView();
        frame = (FrameLayout) view.findViewById(R.id.frame_discounts);

        if (stores != null) {
            init();
        }
    }

    @Override
    public void init() {
        initFragments();
        showStores();
    }

    private void initFragments() {
        fragments = new ArrayList<>();

        DiscountsStoresFragment discountsStoresFragment = new DiscountsStoresFragment();
        discountsStoresFragment.setName("discountStores");
        fragments.add(discountsStoresFragment);

        DiscountsStoreItemsFragment discountsStoreItemsFragment = new DiscountsStoreItemsFragment();
        discountsStoreItemsFragment.setName("discountStoreItems");
        fragments.add(discountsStoreItemsFragment);
    }

    public void showStores() {
        showFragment("discountStores");
    }

    public void showStoreItems(Store store) {
        DiscountsStoreItemsFragment discountsStoreItemsFragment = (DiscountsStoreItemsFragment) showFragment("discountStoreItems");
        //if (discountsStoreItemsFragment != null) {
        discountsStoreItemsFragment.setStore(store);
        discountsStoreItemsFragment.init();
        //}
    }

    @Override
    public void onRemoveItemFromFavorites(Item item) {
        if (isCurrentFragment(this)) {
            for (AppFragment fragment : fragments) {
                fragment.removeItemFromAllLists(item);
            }
        } else {
            init();
        }
    }

    @Override
    public void onAddItemToFavorites(Item item) {
        if (isCurrentFragment(this)) {
            for (AppFragment fragment : fragments) {
                fragment.onAddItemToFavorites(item);
            }
        } else {
            init();
        }
    }

    @Override
    public boolean isDynamic() {
        return true;
    }

    @Override
    public AppFragment getCurrentFragment() {
        return getFragmentByName(showingFragmentName);
    }

    private AppFragment getFragmentByName(String name) {
        for (AppFragment fragment : fragments) {
            if (fragment.getName().equals(name)) {
                return fragment;
            }
        }
        throw new Resources.NotFoundException("fragment with name " + name + " doesn't exist");
    }

    public AppFragment showFragment(String name) {
        if (activity != null && activity.getSupportFragmentManager() != null) {
            Fragment fragment = getFragmentByName(name);
            android.support.v4.app.FragmentTransaction fragmentTransaction = activity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(frame.getId(), fragment);
            //fragmentTransaction.commitNow();
            fragmentTransaction.commitNowAllowingStateLoss();
            //activity.getSupportFragmentManager().executePendingTransactions();
            showingFragmentName = name;
            return (AppFragment) fragment;
        }
        return null;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        DiscountsFragment.activity = (FragmentActivity) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activity = null;
    }

    @Override
    public void removeItemFromAllLists(Item item) {
        DiscountsStoreItemsFragment discountsStoreItemsFragment = (DiscountsStoreItemsFragment) getFragmentByName("discountStoreItems");
        discountsStoreItemsFragment.removeItemFromAllLists(item);

        try {
            show(stores.getItemStore(item));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getShowingFragmentName() {
        return showingFragmentName;
    }

    public void show(Store store) {
        DiscountsStoreItemsFragment discountsStoreItemsFragment = (DiscountsStoreItemsFragment) getFragmentByName("discountStoreItems");
        if (discountsStoreItemsFragment.isEmpty()) {
            showStores();
        } else {
            showStoreItems(store);
        }
    }
}
