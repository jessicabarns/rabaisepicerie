package com.example.jess.rabaisepicerie.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ListView;

import com.example.jess.rabaisepicerie.R;
import com.example.jess.rabaisepicerie.activities.MainActivity;
import com.example.jess.rabaisepicerie.beans.Item;
import com.example.jess.rabaisepicerie.beans.Store;
import com.example.jess.rabaisepicerie.utils.Globals;
import com.example.jess.rabaisepicerie.adapters.ItemListAdapter;

import static com.example.jess.rabaisepicerie.activities.MainActivity.isCurrentFragment;

public class HomeFragment extends AppFragment {
    private static ListView lastChanceList;
    private static ListView biggestDiscountList;
    private static ItemListAdapter lastChanceListAdapter;
    private static ItemListAdapter biggestDiscountListAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.home_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        View view = getView();
        lastChanceList = (ListView) view.findViewById(R.id.last_chance_list);
        lastChanceList.setEmptyView(view.findViewById(R.id.empty_last_chance_list));

        biggestDiscountList = (ListView) view.findViewById(R.id.biggest_discounts_list);
        biggestDiscountList.setEmptyView(view.findViewById(R.id.empty_biggest_discounts_list));

        if(Globals.stores != null){
            init();
        }
    }

    @Override
    public void init() {
        initLastChance();
        initBiggestDiscount();
    }

    private void initBiggestDiscount() {
        if(biggestDiscountList != null){
            biggestDiscountListAdapter = new ItemListAdapter(Globals.stores.getBiggestDiscountItems(), false);
            biggestDiscountList.setAdapter(biggestDiscountListAdapter);
        }
    }

    private void initLastChance() {
        if(lastChanceList != null){
            lastChanceListAdapter = new ItemListAdapter(Globals.stores.getLastChanceItems(), false);
            lastChanceList.setAdapter(lastChanceListAdapter);
        }
    }

    public void removeItemFromAllLists(Item item){
        biggestDiscountListAdapter.remove(item);
        lastChanceListAdapter.remove(item);
    }

    @Override
    public void showStoreItems(Store store) {

    }

    @Override
    public void onRemoveItemFromFavorites(Item item) {
        if(isCurrentFragment(this)){
            lastChanceListAdapter.add(item);
            biggestDiscountListAdapter.add(item);
        }
        else{
            init();
        }
    }

    @Override
    public void onAddItemToFavorites(Item item) {
        if(isCurrentFragment(this)){
            lastChanceListAdapter.remove(item);
            biggestDiscountListAdapter.remove(item);
        }
        else{
            init();
        }
    }

    @Override
    public boolean isDynamic() {
        return false;
    }

    @Override
    public AppFragment getCurrentFragment() {
        return null;
    }

    @Override
    public void showStores() {

    }
}
