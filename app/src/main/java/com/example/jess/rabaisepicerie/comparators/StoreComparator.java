package com.example.jess.rabaisepicerie.comparators;

import com.example.jess.rabaisepicerie.beans.Store;

import java.util.Comparator;

public abstract class StoreComparator implements Comparator<Store>{
    public static final Comparator<Store> sortByName = new Comparator<Store>() {
        @Override
        public int compare(Store store1, Store store2) {
            return store1.getName().compareTo(store2.getName());
        }
    };
}
