package com.example.jess.rabaisepicerie.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jess.rabaisepicerie.R;
import com.example.jess.rabaisepicerie.beans.Item;
import com.example.jess.rabaisepicerie.persist.DatabaseHelper;
import com.example.jess.rabaisepicerie.utils.Globals;
import com.example.jess.rabaisepicerie.utils.ImageLoader;
import com.example.jess.rabaisepicerie.utils.ItemFormatter;

import static com.example.jess.rabaisepicerie.utils.Utils.makeToast;
import static com.example.jess.rabaisepicerie.utils.Utils.strikethrough;

public class ItemDescriptionActivity extends AppCompatActivity implements ImageLoader.ImageLoaderInterface{
    private Item item;

    private TextView name;
    private ImageView imageView;
    private TextView originalPrice;
    private TextView price;
    private TextView discountPercentage;
    private TextView endDate;
    private TextView remainingDays;
    private TextView store;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_description);

        Toolbar menu = (Toolbar) findViewById(R.id.menu);
        setSupportActionBar(menu);

        name = (TextView) findViewById(R.id.name);
        imageView = (ImageView) findViewById(R.id.image);
        originalPrice = (TextView) findViewById(R.id.original_price);
        price = (TextView) findViewById(R.id.price);
        discountPercentage = (TextView) findViewById(R.id.discount_percentage);
        endDate = (TextView) findViewById(R.id.until);
        remainingDays = (TextView) findViewById(R.id.remaining_days);
        store = (TextView) findViewById(R.id.store);

        try {
            item = Globals.stores.getItemById(getIntent().getStringExtra("itemId"));
            initFields();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.preferences:
                toPreferencesActivity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void toPreferencesActivity(){
        startActivity(new Intent(this, PreferencesActvity.class));
    }

    private void initFields() throws Exception {
        loadImage();

        if(item.isFavorite()){
            findViewById(R.id.add).setVisibility(View.GONE);
            findViewById(R.id.remove).setVisibility(View.VISIBLE);
        }

        ItemFormatter itemFormatter = new ItemFormatter(this, item);

        name.setText(itemFormatter.getName());
        originalPrice.setText(itemFormatter.getOriginalPrice());
        strikethrough(originalPrice);
        price.setText(itemFormatter.getPrice());
        discountPercentage.setText(itemFormatter.getDiscountPercentage());
        endDate.setText(itemFormatter.getDiscountEndDate());
        remainingDays.setText(itemFormatter.getRemainingDays());
        store.setText(itemFormatter.getStoreName());
    }

    private void loadImage() {
        ImageLoader imageLoader = new ImageLoader(ItemDescriptionActivity.this, this, item.getImageURL());
        imageLoader.execute();
    }

    @Override
    public void onResponse(Bitmap image) {
        if(image != null){
            imageView.setImageBitmap(image);
        }
    }

    protected void addToFavorites(View view) throws Exception {
        super.onBackPressed();

        String message = getString(R.string.message_item_added, item.getName());
        makeToast(this, message);
        MainActivity.addToFavorites(item);
        DatabaseHelper.saveToFavorites(this, item);
    }

    protected void removeFromFavorites(View view) throws Exception {
        super.onBackPressed();

        String message = getString(R.string.message_item_removed, item.getName());
        makeToast(this, message);
        MainActivity.removeFromFavorites(item);
        DatabaseHelper.removeItemFromFavorites(this, item);
    }

    protected void onBackPressed(View view){
        super.onBackPressed();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {

    }
}
