package com.example.jess.rabaisepicerie.utils;

import com.example.jess.rabaisepicerie.beans.Stores;

public class Globals {
    public static Stores stores;

    public interface Constantes {
        /**
         * Le nombre de jours restants à un rabais pour qu'il apparaisse dans la catégorie "dernière chance"
         */
        int FINISHING_SOON = 365;
        /**
         * Le pourcentage de rabais minimal pour que l'item soit considéré comme ayant un gros rabais
         */
        int BIG_DISCOUNT = 50;
        /**
         * Langue à utiliser si on ne trouve pas la langue locale
         */
        String DEFAULT_LANGUAGE = "en";
        /**
         * table preferences de la base de données
         */
        String PREFERENCES_TABLE_NAME = "preferences";
        String PREFERENCES_TABLE_STORE_NAME_FIELD = "storeName";
        /**
         * table favorites de la base de données
         */
        String FAVORITES_TABLE_NAME = "favorites";
        String FAVORITES_TABLE_ID_FIELD = "id";
        String FAVORITES_TABLE_STORE_NAME_FIELD = "storeName";
        /**
         * L'url du fichier json contenant les rabais
         */
        String JSON_URL = "http://www.json-generator.com/api/json/get/cniIUQkRqq";
        /**
         * indexs du fichier JSON des rabais
         */
        String STORES = "stores";
        String ID = "id";
        String NAME = "name";
        String ITEMS = "items";
        String IMAGE = "image";
        String DISCOUNT_PERCENTAGE = "discountPercentage";
        String ORIGINAL_PRICE = "originalPrice";
        String START_DATE = "startDate";
        String END_DATE = "endDate";
        /**
         * Nom des fragments
         */
        String LIST_STORES_FRAGMENT_NAME = "listStores";
        String LIST_STORE_ITEMS_FRAGMENT_NAME = "listStoreItems";
        String LIST_ALL_ITEMS_FRAGMENT_NAME = "listAllStoreItems";

        /**
         * Erreurs
         */
        String FRAGMENT_NAME_DOESNT_EXIST_ERROR = "The fragment with name %s doesn't exist";
        String DATABASE_UPGRADE_UNKNOWN_OLDVERSION_ERROR = "onUpgrade() with unknown oldVersion %d";
        String ITEM_IN_NO_STORE_ERROR = "The item is in no store";
    }
}
