package com.example.jess.rabaisepicerie.comparators;

import com.example.jess.rabaisepicerie.beans.Item;

import java.util.Comparator;

public abstract class ItemComparator implements Comparator<Item> {
    public static final Comparator<Item> sortByDiscountEndDate = new Comparator<Item>() {
        @Override
        public int compare(Item item1, Item item2) {
           return item1.getDiscountEndDate().compareTo(item2.getDiscountEndDate());
        }
    };

    public static final Comparator<Item> sortByBiggestDiscountPercentage = new Comparator<Item>() {
        @Override
        public int compare(Item item1, Item item2) {
            if(item1.getDiscountPercentage() > item2.getDiscountPercentage()){
                return 1;
            }
            else if(item1.getDiscountPercentage() < item2.getDiscountPercentage()){
                return -1;
            }
            return 0;
        }
    };
}
