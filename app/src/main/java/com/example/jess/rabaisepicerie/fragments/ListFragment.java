package com.example.jess.rabaisepicerie.fragments;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.example.jess.rabaisepicerie.R;
import com.example.jess.rabaisepicerie.beans.Item;
import com.example.jess.rabaisepicerie.beans.Store;
import com.example.jess.rabaisepicerie.utils.Globals;

import java.util.ArrayList;
import java.util.List;

import static com.example.jess.rabaisepicerie.activities.MainActivity.isCurrentFragment;
import static com.example.jess.rabaisepicerie.utils.Globals.Constantes.FRAGMENT_NAME_DOESNT_EXIST_ERROR;
import static com.example.jess.rabaisepicerie.utils.Globals.Constantes.LIST_ALL_ITEMS_FRAGMENT_NAME;
import static com.example.jess.rabaisepicerie.utils.Globals.Constantes.LIST_STORES_FRAGMENT_NAME;
import static com.example.jess.rabaisepicerie.utils.Globals.Constantes.LIST_STORE_ITEMS_FRAGMENT_NAME;
import static com.example.jess.rabaisepicerie.utils.Globals.stores;

public class ListFragment extends AppFragment {
    private static List<AppFragment> fragments;
    private static FrameLayout frame;
    private static String showingFragmentName;
    private static FragmentActivity activity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        View view = getView();
        frame = (FrameLayout) view.findViewById(R.id.frame_list);

        if (stores != null) {
            init();
        }
    }

    @Override
    public void init() {
        initFragments();
        showStores();
    }

    private void initFragments() {
        fragments = new ArrayList<>();

        ListStoresFragment listStoresFragment = new ListStoresFragment();
        listStoresFragment.setName(LIST_STORES_FRAGMENT_NAME);
        fragments.add(listStoresFragment);

        ListStoreItemsFragment listStoreItemsFragment = new ListStoreItemsFragment();
        listStoreItemsFragment.setName(LIST_STORE_ITEMS_FRAGMENT_NAME);
        fragments.add(listStoreItemsFragment);

        ListStoreItemsFragment listAllItemsFragment = new ListStoreItemsFragment();
        listAllItemsFragment.setName(LIST_ALL_ITEMS_FRAGMENT_NAME);
        fragments.add(listAllItemsFragment);
    }

    public void showStores() {
        showFragment(LIST_STORES_FRAGMENT_NAME);
    }

    public void showStoreItems(Store store) {
        ListStoreItemsFragment listStoreItemsFragment = (ListStoreItemsFragment) showFragment(LIST_STORE_ITEMS_FRAGMENT_NAME);
        listStoreItemsFragment.setStore(store);
        listStoreItemsFragment.init();
    }

    @Override
    public void onRemoveItemFromFavorites(Item item) {
        if(isCurrentFragment(this)){
            for(AppFragment fragment : fragments){
                fragment.onRemoveItemFromFavorites(item);
            }
            try {
                show(stores.getItemStore(item));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else{
            init();
        }
    }

    @Override
    public void onAddItemToFavorites(Item item) {
        if(isCurrentFragment(this)){
            for(AppFragment fragment : fragments){
                fragment.onAddItemToFavorites(item);
            }
        }
        else{
            init();
        }
    }

    @Override
    public boolean isDynamic() {
        return true;
    }

    @Override
    public AppFragment getCurrentFragment() {
        return getFragmentByName(showingFragmentName);
    }

    private AppFragment getFragmentByName(String name){
        for(AppFragment fragment : fragments){
            if(fragment.getName().equals(name)){
                return fragment;
            }
        }
        throw new Resources.NotFoundException(String.format(FRAGMENT_NAME_DOESNT_EXIST_ERROR, name));
    }

    public AppFragment showFragment(String name) {
        if (activity != null && activity.getSupportFragmentManager() != null && frame != null) {
            Fragment fragment = getFragmentByName(name);
            android.support.v4.app.FragmentTransaction fragmentTransaction = activity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(frame.getId(), fragment);
            fragmentTransaction.commitAllowingStateLoss();
            showingFragmentName = name;
            return (AppFragment) fragment;
        }
        return null;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ListFragment.activity = (FragmentActivity) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activity = null;
    }

    public void show(Store store) {
        ListStoreItemsFragment listStoreItemsFragment = (ListStoreItemsFragment) getFragmentByName(LIST_STORE_ITEMS_FRAGMENT_NAME);
        if (listStoreItemsFragment.isEmpty()) {
            showStores();
        } else {
            showStoreItems(store);
        }
    }

    @Override
    public void removeItemFromAllLists(Item item) {
        ListStoreItemsFragment listStoreItemsFragment = (ListStoreItemsFragment) getFragmentByName(LIST_STORE_ITEMS_FRAGMENT_NAME);
        listStoreItemsFragment.removeItemFromAllLists(item);

        try {
            show(stores.getItemStore(item));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showAllItems() {
        ListStoreItemsFragment listAllStoreItemsFragment = (ListStoreItemsFragment) showFragment(LIST_ALL_ITEMS_FRAGMENT_NAME);
        Store store = new Store("0", getString(R.string.my_items), Globals.stores.getFavoriteItems());
        listAllStoreItemsFragment.setStore(store);
        listAllStoreItemsFragment.init();
    }
}
