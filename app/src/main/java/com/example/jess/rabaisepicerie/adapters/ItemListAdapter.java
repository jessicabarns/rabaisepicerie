package com.example.jess.rabaisepicerie.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.jess.rabaisepicerie.R;
import com.example.jess.rabaisepicerie.beans.Item;
import com.example.jess.rabaisepicerie.utils.ItemFormatter;

import java.util.List;

import static android.graphics.Paint.STRIKE_THRU_TEXT_FLAG;
import static com.example.jess.rabaisepicerie.utils.Utils.strikethrough;

public class ItemListAdapter extends BaseAdapter{
    private List<Item> items;
    private boolean removeButton;
    private LayoutInflater inflater;

    public ItemListAdapter(List<Item> items, boolean removeButton){
        this.items = items;
        this.removeButton = removeButton;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Item getItem(int position) {
       return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(inflater == null){
            Context context = parent.getContext();
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        //LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(R.layout.item_list_view, parent, false);

        Item item = getItem(position);
        ItemFormatter itemFormatter = new ItemFormatter(inflater.getContext(), item);

        TextView name = (TextView) row.findViewById(R.id.name);
        TextView originalPrice = (TextView) row.findViewById(R.id.original_price);
        TextView discountPrice = (TextView) row.findViewById(R.id.discount_price);
        TextView remainingDays = (TextView) row.findViewById(R.id.remaining_days);
        ImageButton add = (ImageButton) row.findViewById(R.id.add);
        ImageButton see = (ImageButton) row.findViewById(R.id.see);
        ImageButton remove = (ImageButton) row.findViewById(R.id.remove);

        add.setTag(item);
        see.setTag(item);
        remove.setTag(item);

        if(removeButton){
            add.setVisibility(View.GONE);
            remove.setVisibility(View.VISIBLE);
        }

        name.setText(itemFormatter.getName());
        originalPrice.setText(itemFormatter.getOriginalPrice());
        strikethrough(originalPrice);
        discountPrice.setText(itemFormatter.getPrice());
        remainingDays.setText(itemFormatter.getRemainingDays());

        return (row);
    }

    public void remove(Item item) {
        items.remove(item);
        notifyDataSetChanged();
    }

    public void add(Item item) {
        items.add(item);
        notifyDataSetChanged();
    }
}
