package com.example.jess.rabaisepicerie.beans;

import com.example.jess.rabaisepicerie.comparators.ItemComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.example.jess.rabaisepicerie.comparators.StoreComparator.sortByName;
import static com.example.jess.rabaisepicerie.utils.Globals.Constantes.BIG_DISCOUNT;
import static com.example.jess.rabaisepicerie.utils.Globals.Constantes.FINISHING_SOON;
import static com.example.jess.rabaisepicerie.utils.Globals.Constantes.ITEM_IN_NO_STORE_ERROR;

public class Stores {
    private List<Store> stores;

    public Stores(List<Store> stores) {
        this.stores = stores;
        Collections.sort(stores, sortByName);
    }

    public List<Item> getLastChanceItems() {
        List<Item> items = new ArrayList<>();
        for (Store store : getVisibleStores()) {
            for (Item item : store.getNonFavoriteItems()) {
                if (item.getDiscountRemainingDays() <= FINISHING_SOON) {
                    items.add(item);
                }
            }
        }

        Collections.sort(items, ItemComparator.sortByDiscountEndDate);
        return items;
    }

    public List<Item> getBiggestDiscountItems() {
        List<Item> items = new ArrayList<>();
        for (Store store : getVisibleStores()) {
            for (Item item : store.getNonFavoriteItems()) {
                if (item.getDiscountPercentage() >= BIG_DISCOUNT) {
                    items.add(item);
                }
            }
        }

        Collections.sort(items, ItemComparator.sortByBiggestDiscountPercentage);
        return items;
    }

    public List<Store> getStores() {
        return stores;
    }

    public Item getItemById(String id) throws Exception {
        for (Store store : stores) {
            for (Item item : store.getValidDiscountItems()) {
                if (item.getId().equals(id)) {
                    return item;
                }
            }
        }
        return null;
    }

    public Store getItemStore(Item item) throws Exception {
        for (Store store : stores) {
            if (store.getDiscountItems().contains(item)) {
                return store;
            }
        }
        throw new Exception(ITEM_IN_NO_STORE_ERROR);
    }

    public List<Store> getVisibleStores() {
        List<Store> visibleStores = new ArrayList<>();
        for (Store store : stores) {
            if (store.isVisible()) {
                visibleStores.add(store);
            }
        }
        return visibleStores;
    }

    public int getNumberOfLovedItems() {
        int number = 0;
        for (Store store : getVisibleStores()) {
            number += store.getNumberOfFavoritesItems();
        }
        return number;
    }

    public double getCostOfLovedItems() {
        double cost = 0;
        for (Store store : getVisibleStores()) {
            for (Item item : store.getFavoriteItems()) {
                cost += item.getDiscountedPrice();
            }
        }
        return cost;
    }

    public List<Store> getFavouriteStores() {
        List<Store> favorites = new ArrayList<>();
        for (Store store : getVisibleStores()) {
            if (store.hasFavouritesItems()) {
                favorites.add(store);
            }
        }
        return favorites;
    }

    public List<Item> getFavoriteItems() {
        List<Item> items = new ArrayList<>();
        for (Store store : getFavouriteStores()) {
            items.addAll(store.getFavoriteItems());
        }
        return items;
    }

    @Override
    public String toString() {
        return "Stores{" +
                "stores=" + stores +
                '}';
    }
}
