package com.example.jess.rabaisepicerie.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.jess.rabaisepicerie.R;
import com.example.jess.rabaisepicerie.beans.Store;
import com.example.jess.rabaisepicerie.persist.DatabaseHelper;
import com.example.jess.rabaisepicerie.utils.Globals;
import com.example.jess.rabaisepicerie.utils.Utils;

import java.util.HashSet;
import java.util.Set;

import static com.example.jess.rabaisepicerie.utils.Globals.Constantes.PREFERENCES_TABLE_NAME;
import static com.example.jess.rabaisepicerie.utils.Utils.makeToast;

public class PreferencesActvity extends AppCompatActivity {
    private LinearLayout checkboxesLayout;
    private Set<String> checkedStores;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferences);

        checkboxesLayout = (LinearLayout) findViewById(R.id.stores_layout);
        Toolbar menu = (Toolbar) findViewById(R.id.menu);

        setSupportActionBar(menu);
        checkedStores = DatabaseHelper.getPreferences(this);
        initCheckboxes();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.refresh:
                //MainActivity.fetchStores();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initCheckboxes() {
        CheckBox checkBox;
        for (Store store : Globals.stores.getStores()) {
            checkBox = new CheckBox(this);
            checkBox.setText(store.getName());
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        checkedStores.add((String) buttonView.getText());
                    } else {
                        checkedStores.remove((String) buttonView.getText());
                    }
                }
            });

            if (checkedStores.contains(store.getName())) {
                checkBox.setChecked(true);
            }

            checkboxesLayout.addView(checkBox);
        }
    }

    protected void onSavePreferences(View view) throws Exception {
        if (arePreferencesOk()) {
            for (Store store : Globals.stores.getStores()) {
                store.setVisible(isStoreChecked(store));
            }
            savePreferences();
            super.onBackPressed();
        } else {
            makeToast(this, getString(R.string.preferences_save_error));
        }
    }

    private boolean arePreferencesOk() {
        return checkedStores.size() > 0;
    }

    private boolean isStoreChecked(Store store) {
        return checkedStores.contains(store.getName());
    }

    private void savePreferences() throws Exception {
        /*SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sharedPreferences.edit().putStringSet("stores", checkedStores).apply();*/
        DatabaseHelper.savePreferences(this, checkedStores);
        MainActivity.refreshStores(DatabaseHelper.getPreferences(this));
        makeToast(this, getString(R.string.preferences_saved));
    }

    protected void onDefaultPreferences(View view) throws Exception {
        for (Store store : Globals.stores.getStores()) {
            if(!isStoreChecked(store)){
                checkedStores.add(store.getName());
            }
            store.setVisible(true);
        }
        savePreferences();
        super.onBackPressed();
    }

    protected void onResetPreferences(View view) {
        //PreferenceManager.getDefaultSharedPreferences(this).edit().clear().apply();
        DatabaseHelper.deleteAllFromTable(this, PREFERENCES_TABLE_NAME);
        resetActivity();
        makeToast(this, getString(R.string.preferences_reset));
    }

    private void resetActivity() {
        checkedStores.clear();

        View child;
        CheckBox checkBox;
        for (int i = 0; i < checkboxesLayout.getChildCount(); i++) {
            try {
                child = checkboxesLayout.getChildAt(i);
                checkBox = (CheckBox) child;
                checkBox.setChecked(false);
            } catch (Exception ignored) {

            }
        }
    }

    @Override
    public void onBackPressed() {
        if (!DatabaseHelper.arePreferencesSet(this)) {
            makeToast(this, getString(R.string.preferences_not_set_error));
        } else {
            notSavedAlert();
        }
    }

    private void notSavedAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.alert_preferences_not_saved);
        builder.setCancelable(true);

        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        builder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }
}
